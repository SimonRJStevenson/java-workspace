package com.citi.training.library.service;

import java.util.List;
import java.util.Optional;

import com.citi.training.library.doa.LibraryMongoRepo;
import com.citi.training.library.model.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LibraryService {

    @Autowired
    private LibraryMongoRepo mongoRepo;

    //add new item
    public Item addItem(Item item){
        item.setBorrowed(false);
        return mongoRepo.save(item);
    }

    //find item
    public Item findItem(String itemID){
        Optional<Item> optItem = mongoRepo.findById(itemID);
        if(optItem.isPresent()){
            return optItem.get();
        } else {
            return null;
        }
    }

    //remove item
    public void removeItem(String itemID){
         mongoRepo.deleteById(itemID);
    }



    //borrow item - CR Update D
    public Item borrowItem(String itemID){

        Item itemToUpdate = mongoRepo.findOneByID(itemID);

        itemToUpdate.setBorrowed(true);
        return mongoRepo.save(itemToUpdate);
        

    }

    //return item - CR Update D
    public Item returnItem(String itemID, Boolean bool){
        
        return null;


    }


    

    //find all items
    public List<Item> searchItem(){
        return mongoRepo.findAll();
    }

    //return item
    
}
