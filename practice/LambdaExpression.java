public class LambdaExpression{

    public static void main(String[] args) {
        
        MyFunction myFunction = () -> System.out.println("Working !"); 
        
        myFunction.apply();
    }
}