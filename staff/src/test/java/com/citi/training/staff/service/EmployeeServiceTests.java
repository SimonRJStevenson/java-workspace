package com.citi.training.staff.service;

import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class EmployeeServiceTests {
    
    @Autowired
    EmployeeService employeeService;

    @Test
    public void test_save_sanityCheck(){

        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. Test McTesty");
        testEmployee.setAddress("123 Main Street");

        employeeService.save(testEmployee);


    }

    @Test
    public void test_findAll_sanityCheck(){
        assert(employeeService.findAll().size() > 0);  
    }

}
