package com.citi.training.library.model;



public class Item {

    private boolean isBorrowed;
    private String name;
    private ItemType itemType;
    private String id;

    public Item(boolean isBorrowed, String name, ItemType itemType, String id) {
        this.isBorrowed = isBorrowed;
        this.name = name;
        this.itemType = itemType;
        this.id = id;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean isBorrowed) {
        this.isBorrowed = isBorrowed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    
    

    
}
