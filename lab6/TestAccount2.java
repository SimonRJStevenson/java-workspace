public class TestAccount2 {

    public static void main(String[] args) {

         //lab 8

         Account[] arrayOfAccounts = new Account[5];

         double[] amounts = {23,5444,2,345,34};
         String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
 
         for ( int loop = 0; loop<arrayOfAccounts.length; loop++){
 
            arrayOfAccounts[loop] = new Account(amounts[loop], names[loop]);

 
             System.out.println("Account Holder : " + arrayOfAccounts[loop].getName() + "\n"
             + "Account Balance : " + arrayOfAccounts[loop].getBalance());
 
             arrayOfAccounts[loop].addInterest();
             System.out.println("Current balance : " + arrayOfAccounts[loop].getBalance());
             
         }

 
         System.out.println("\n*************\n");
        
    }
    
}
