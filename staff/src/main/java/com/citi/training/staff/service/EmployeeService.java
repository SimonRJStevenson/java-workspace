package com.citi.training.staff.service;

import java.util.List;

import com.citi.training.staff.dao.EmployeeMongoRepo;
import com.citi.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//Save employee objects and get employee objects
@Component
public class EmployeeService {
    

    @Autowired
    private EmployeeMongoRepo mongoRepo;

    //Find all and save as list
    public List<Employee> findAll() {

        return mongoRepo.findAll();
    }

    //Save employee object to database
    public Employee save(Employee employee){
        return mongoRepo.save(employee);
    }

}
