package com.citi.training.library.doa;

import com.citi.training.library.model.Item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LibraryMongoRepo extends MongoRepository<Item, String> {

    public Item findOneByID(String id);
    
}
