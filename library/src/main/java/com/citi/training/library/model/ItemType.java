package com.citi.training.library.model;

public enum ItemType {

    BOOKS, CDS, DVDS, PERIODICALS;
    
}
