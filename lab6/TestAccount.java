public class TestAccount{

    public static void main(String[] args) {
        
        //lab 6
        Account myAccount = new Account();

        myAccount.setName("Simon");
        myAccount.setBalance(500000);

        System.out.println("Account Holder : " + myAccount.getName() + "\n"
         + "Account Balance : " + myAccount.getBalance());

        
        myAccount.addInterest();
        System.out.println("Current balance : " + myAccount.getBalance());

        System.out.println("\n*************\n");

        //lab 7

        Account[] arrayofAccounts = new Account[5];

        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for ( int loop = 0; loop<arrayofAccounts.length; loop++){

            arrayofAccounts[loop] = new Account();
            arrayofAccounts[loop].setBalance(amounts[loop]);
            arrayofAccounts[loop].setName(names[loop]);

            System.out.println("Account Holder : " + arrayofAccounts[loop].getName() + "\n"
            + "Account Balance : " + arrayofAccounts[loop].getBalance());

            arrayofAccounts[loop].addInterest();
            System.out.println("Current balance : " + arrayofAccounts[loop].getBalance());
            
        }

        System.out.println("\n*************\n");

    }
}