import java.util.Random;

public class MyFirstClass{


    public static void main(String[] args) {
        
        String make = "Mini";
        String model = "Cooper";
        double engineSize = 1.5;
        byte gear = 3;
        int speed;
        int rpm;

        System.out.println("The make is " +  make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if (engineSize <= 1.3){
            System.out.println("Car is weak");
        } else {
            System.out.println("Car is powerful");
        }

        Random rand = new Random();

        switch(gear) {

            case 1 :
                speed = (short) 0 + rand.nextInt(15);

                break;

            case 2 : 

                speed = (short) 15 + rand.nextInt(25);

                break;

            case 3 : 

                speed = (short) 25 + rand.nextInt(35);

                break;

            case 4 : 

                speed = (short) rand.nextInt(50);

                break; 
                
            case 5 : 

                speed = (short) rand.nextInt(60);

                break;

            case 6 : 

                speed = (short) rand.nextInt(80);

                break;

            default:
                speed = 0;
                break;
        }
        
        System.out.println("Speed is " + speed);
        rpm = Math.multiplyExact(speed, 50);
        System.out.println("RPM is " + rpm);
    }

}